import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | imperialism-year', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:imperialism-year');
    assert.ok(route);
  });
});
