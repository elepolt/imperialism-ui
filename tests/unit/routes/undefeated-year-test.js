import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | undefeated-year', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:undefeated-year');
    assert.ok(route);
  });
});
