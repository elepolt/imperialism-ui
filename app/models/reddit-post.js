import DS from 'ember-data';
const { Model, attr } = DS;

export default class RedditPostModel extends Model {
  @attr created;
  @attr title;
  @attr redditId;
  @attr redditCreateDate;
  @attr body;
  @attr imageUrl;
  @attr thumbnailUrl;
  @attr permalink;
}
