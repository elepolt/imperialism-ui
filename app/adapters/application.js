import DS from 'ember-data';
import config from '../config/environment';

export default class ApplicationAdapter extends DS.JSONAPIAdapter {
  host = config.host;
}
