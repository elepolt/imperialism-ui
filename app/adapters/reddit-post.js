import ApplicationAdapter from './application';

export default class RedditPostAdapter extends ApplicationAdapter {
  query(store, type, query) {
    var url = this.buildURL(type.modelName, null, null, 'query', query);
    return this.ajax(url, 'GET');
  }
  urlForQuery(query, modelName) {
    let post_type = query.post_type;
    query = null;
    if (post_type) {
      return `${this.urlPrefix()}/${modelName}s/${post_type}`;
    } else {
      return super.urlForQuery(...arguments);
    }
  }
}
