import Route from '@ember/routing/route';

export default class ImperialismYearRoute extends Route {
  model(params) {
    let year = params.year;
    return this.store.query('imperialism', { year: year });
  }
}
