import Route from '@ember/routing/route';

export default class UndefeatedYearRoute extends Route {
  model(params) {
    let year = params.year;
    return this.store.query('undefeated', { year: year });
  }
}
