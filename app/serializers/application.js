import DS from 'ember-data';
import { underscore } from '@ember/string';

export default class ApplicationSerializer extends DS.JSONAPISerializer {
  // Attributes from Django come as underscored
  keyForAttribute(key) {
    return underscore(key);
  }
}
